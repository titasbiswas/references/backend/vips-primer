package pvt.titas.vips;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VipsApplication {

    public static void main(String[] args) {
        SpringApplication.run(VipsApplication.class, args);
    }

}
