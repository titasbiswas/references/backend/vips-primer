package pvt.titas.vips.simulation;

import com.criteo.vips.VipsContext;
import com.criteo.vips.VipsException;
import com.criteo.vips.VipsImage;
import com.criteo.vips.enums.VipsImageFormat;
import pvt.titas.vips.service.impl.VipsImageProcessor;
import pvt.titas.vips.utils.VipsTestUtils;

import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.FileSystems;

public class SimpleExample {

    static String in_path = "/home/tbiswas/sample.tiff";
    static String out_path = "/home/tbiswas/sample_out.jpg";

    public static void testSimple() {
        // Set vips memory leak report at exit
        VipsContext.setLeak(true);
        VipsImage image = null;
        VipsImage metaStripedImage = null;
        String fileSeparator = FileSystems.getDefault().getSeparator();
        try {
            byte[] contents = VipsTestUtils.readFromFile(in_path);
           VipsImageProcessor processor = new VipsImageProcessor();
           byte[] outContents = processor.scaleKeepingAspectRatio(900, contents, false);

            System.out.println(contents.length);
            VipsTestUtils.writeToFile(out_path, outContents);

        } catch (IOException | VipsException e) {
            e.printStackTrace();
        }
    }
}
