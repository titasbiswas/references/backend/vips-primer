/*
  Copyright (c) 2019 Criteo

  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
*/

package pvt.titas.vips.utils;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class VipsTestUtils {
    public static byte[] getByteArray(String filename) throws IOException {
        ClassLoader classLoader = VipsTestUtils.class.getClassLoader();
        filename = classLoader.getResource(filename).getFile();
        return Files.readAllBytes(new File(filename).toPath());
    }

    public static void writeToFile(String outputFilePath, byte[] bytes) throws IOException {
        Path path = Paths.get(outputFilePath);
        Files.deleteIfExists(path);
        Files.createFile(path);
        Files.write(path, bytes);
    }

    public static byte[] readFromFile(String inputFilePath) throws IOException {
        Path path = Paths.get(inputFilePath);
        return Files.readAllBytes(path);
    }

    public static ByteBuffer getDirectByteBuffer(String filename) throws IOException {
        byte[] bytes = getByteArray(filename);
        ByteBuffer buffer = ByteBuffer.allocateDirect(bytes.length);
        buffer.put(bytes, 0, bytes.length);
        return buffer;
    }
}
