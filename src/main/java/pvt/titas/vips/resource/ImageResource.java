package pvt.titas.vips.resource;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import pvt.titas.vips.service.api.ImageProcessor;

import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.channels.ReadableByteChannel;

import static java.nio.channels.Channels.newChannel;

@RestController
public class ImageResource {

    private ImageProcessor _imageProcessor;

    @Autowired
    public void setImageProcessor(ImageProcessor imageProcessor){
        this._imageProcessor = imageProcessor;
    }

    @PostMapping(path = "/image")
    protected ResponseEntity uploadImage(@RequestParam("file") MultipartFile file) throws IOException {
       /* InputStream in = file.getInputStream();
        ByteBuffer byteBuffer = ByteBuffer.allocate(in.available());
        ReadableByteChannel channel = newChannel(in);
        IOUtils.readFully(channel, byteBuffer);*/

        byte[] inputImageData = file.getBytes();
        byte[] processedImageData = _imageProcessor.scaleKeepingAspectRatio(900, inputImageData, false);
        return ResponseEntity.ok(processedImageData.length);
    }

}
