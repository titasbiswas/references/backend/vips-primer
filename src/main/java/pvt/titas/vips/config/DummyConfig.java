package pvt.titas.vips.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pvt.titas.vips.service.impl.VipsImageProcessor;
import pvt.titas.vips.simulation.SimpleExample;

@Configuration
public class DummyConfig {

   @Bean
    public VipsImageProcessor vipsImageProcessor() {
        SimpleExample.testSimple();
        return new VipsImageProcessor();
    }
}
