package pvt.titas.vips.service.api;

public interface ImageProcessor {
    byte [] scaleKeepingAspectRatio(int maxDim, byte[] imageData, boolean stripMeta);
}
