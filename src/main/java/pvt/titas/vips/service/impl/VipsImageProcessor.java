package pvt.titas.vips.service.impl;

import com.criteo.vips.VipsImage;
import com.criteo.vips.enums.VipsImageFormat;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.awt.*;

//@Component
//@Primary
public class VipsImageProcessor implements pvt.titas.vips.service.api.ImageProcessor {

    @Override
    public byte [] scaleKeepingAspectRatio(int maxDim, byte[] imageData, boolean stripMeta){
        VipsImage image=null;
        byte[] outData=null;
        try {
            image = new VipsImage(imageData, imageData.length);
            image = scale(image, maxDim, maxDim, false);
            outData =  image.writeToArray(VipsImageFormat.JPG, stripMeta);

        }catch (Exception e){
            e.printStackTrace();
        }
        finally {
            image.release();
        }
        return outData;
    }

    private VipsImage scale(VipsImage image, int width, int height, boolean forceScale) {
        Long startAt = System.currentTimeMillis();

        Dimension destDimension = new Dimension(width, height);
        image.resize(destDimension, forceScale);

        Long finishedAt = System.currentTimeMillis();


        return image;
    }
}
